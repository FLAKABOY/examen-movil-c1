package com.example.examenc1;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    private EditText txtUsuario, txtBase, txtAltura;
    private Button btnSiguiente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUsuario = findViewById(R.id.txtUsuario);
        txtBase = findViewById(R.id.txtBase);
        txtAltura = findViewById(R.id.txtAltura);
        btnSiguiente = findViewById(R.id.btnSiguiente);

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = txtUsuario.getText().toString();
                String baseStr = txtBase.getText().toString();
                String alturaStr = txtAltura.getText().toString();

                if (!baseStr.isEmpty() && !alturaStr.isEmpty()) {
                    try {
                        float base = Float.parseFloat(baseStr);
                        float altura = Float.parseFloat(alturaStr);

                        Intent intent = new Intent(MainActivity.this, CalcularRectangulo.class);
                        intent.putExtra("nombre", username);
                        intent.putExtra("base", base);
                        intent.putExtra("altura", altura);
                        startActivity(intent);
                    } catch (NumberFormatException e) {
                        Toast.makeText(MainActivity.this, "Ingrese valores válidos para la base y la altura", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Ingrese la base y la altura", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
