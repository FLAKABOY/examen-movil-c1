package com.example.examenc1;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class CalcularRectangulo extends AppCompatActivity {

    private TextView textViewNombre, textViewBase, textViewAltura, textViewResultado;
    private RadioGroup radioGroupCalculos;
    private Button buttonCalcular, buttonRegresar;

    private float base, altura;
    private String nombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        textViewNombre = findViewById(R.id.textViewNombre);
        textViewBase = findViewById(R.id.textViewBase);
        textViewAltura = findViewById(R.id.textViewAltura);
        textViewResultado = findViewById(R.id.textViewResultado);
        radioGroupCalculos = findViewById(R.id.radioGroupCalculos);
        buttonCalcular = findViewById(R.id.buttonCalcular);
        buttonRegresar = findViewById(R.id.buttonRegresar);

        Intent intent = getIntent();
        nombre = intent.getStringExtra("nombre");
        base = intent.getFloatExtra("base", 0);
        altura = intent.getFloatExtra("altura", 0);

        textViewNombre.setText("Nombre: " + nombre);
        textViewBase.setText("Base: " + base);
        textViewAltura.setText("Altura: " + altura);

        buttonCalcular.setOnClickListener(v -> {
            int selectedId = radioGroupCalculos.getCheckedRadioButtonId();
            Rectangulo rectangulo = new Rectangulo(base, altura);
            String resultado = "";

            if (selectedId == R.id.radioButtonArea) {
                resultado = "El área es: " + rectangulo.calcularArea();
            } else if (selectedId == R.id.radioButtonPerimetro) {
                resultado = "El perímetro es: " + rectangulo.calcularPerimetro();
            }

            textViewResultado.setText(resultado);
        });

        buttonRegresar.setOnClickListener(v -> finish());
    }
}

